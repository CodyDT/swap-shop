from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import listings
import os

app = FastAPI()
app.include_router(listings.router)
